import '../styles/globals.css'
import '../public/fonts/_fonts.css'
import Menu from '../components/menu'

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Component {...pageProps} />
      <Menu />
    </>
  );
}

export default MyApp
