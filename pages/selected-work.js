import { useState, useEffect, useRef } from 'react';
import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/selected-work.module.css'

import Menu from '../components/menu'
import BrowserRender from '../components/browser-render'


export default function SelectedWork() {
  return (
    (<div className={styles.container}>
      <Head>
        <title>Sebastian Gale - Creative Web Developer | Selected Work</title>
        <meta name="description" key="description" content="I build and design uniquely great websites for businesses and independent professionals. I’m a web developer and designer based out of Victoria, Canada who speaks both English and French. I’ve been doing software development as a hobby for over 7 years now, including 5 years of professional experience."/>

        <link rel="icon" href="/favicon.ico?v=JyAdPKv4m5" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=JyAdPKv4m5"/>
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=JyAdPKv4m5"/>
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=JyAdPKv4m5"/>
        <link rel="manifest" href="/site.webmanifest?v=JyAdPKv4m5"/>
        <link rel="mask-icon" href="/safari-pinned-tab.svg?v=JyAdPKv4m5" color="#808080"/>
        <link rel="shortcut icon" href="/favicon.ico?v=JyAdPKv4m5"/>
        <meta name="msapplication-TileColor" content="#f8f8f8"/>
        <meta name="theme-color" content="#f8f8f8"/>
      </Head>
      <nav className={styles.nav}>
        <span className={styles.logotype}><Link href="/">SG</Link></span>
        <div className={styles.nav_links}>
          <Link href="/selected-work" className={styles.current_link}>Selected Work.</Link>
          <Link href="/info">Info.</Link>
          <Link href="/quote">Get a Quote.</Link>
        </div>
      </nav>
      <main className={styles.main}>
        <h1 className={styles.page_title}>Selected Work.</h1>

        <div className={styles.project}>
          <div>
            <h2 className={styles.project_title}>Garrett Shannon</h2>
            <p className={styles.project_text}>“I am THRILLED with how it all came together. You did an excellent job, you were very easy to communicate with and it was very helpful being able to ask you questions / advice about best practices.” - Garrett Shannon</p>
          </div>
          <div className={styles.image_container}>
            <BrowserRender/>
            <img className={styles.project_image} src="/portfolio/garrettshannon.com.png" alt="screenshot of garrettshannon.com" />
          </div>
          <p className={styles.project_text}>I designed and developed a portfolio website from scratch for Garrett Shannon, a director of photography based out of Texas, US.</p>
          <p className={styles.project_link}><a href="https://garrettshannon.com" target="_blank"><u>View live website.</u></a></p>
        </div>

        <div className={styles.project}>
          <div>
            <h2 className={styles.project_title}>Quantum Energy Squares</h2>
          </div>
          <div className={styles.image_container}>
            <BrowserRender/>
            <img className={styles.project_image} src="/portfolio/quantumsquares.com.png" alt="screenshot of quantumsquares.com" />
          </div>
          <p className={styles.project_text}>I was the main developer handling ongoing development of the website for a grocery brand called Quantum Energy Squares, as well as a bit of design work. This included custom theme development for different checkout flows, landing pages, promotions, etc.</p>
          <p className={styles.project_link}><a href="https://quantumsquares.com" target="_blank"><u>View live website.</u></a></p>
        </div>

        <div className={styles.project}>
          <div>
            <h2 className={styles.project_title}>Studyverse</h2>
          </div>
          <div className={styles.image_container}>
            <BrowserRender/>
            <img className={styles.project_image} src="/portfolio/studyverse.live_blog.png" alt="screenshot of studyverse.live_blog" />
          </div>
          <p className={styles.project_text}>I was contracted to build the mobile version of the web app for an edtech startup called Studyverse. I also implemented a blog for their website (using a design they provided), as well as some technical optimization for speed and SEO.</p>
          <p className={styles.project_link}><a href="https://studyverse.live/blog" target="_blank"><u>View live website.</u></a></p>
        </div>

        <div className={styles.project}>
          <div>
            <h2 className={styles.project_title}>JLH Capital Partners</h2>
            <p className={styles.project_text}>“I’m very pleased with how the page turned out, Sebastian was great to work with and I’ll look to him for any future web needs for our business moving forward” - G Benjamin</p>
          </div>
          <div className={styles.image_container}>
            <BrowserRender/>
            <img className={styles.project_image} src="/portfolio/featured.jlhcapitalpartners.com.png" alt="screenshot of featured.jlhcapitalpartners.com" />
          </div>
          <p className={styles.project_text}>I worked with a designer and implemented their design of a new “Featured Turnkey” page for a real estate company based out of the US.</p>
        </div>

        <div className={styles.project}>
          <div>
            <h2 className={styles.project_title}>AmberSky Marketing Ltd</h2>
          </div>
          <div className={styles.image_container}>
            <BrowserRender/>
            <img className={styles.project_image} src="/portfolio/www.amberskymarketing.com.png" alt="screenshot of amberskymarketing.com" />
          </div>
          <p className={styles.project_text}>I worked with a marketing agency to implement their design of their own website.</p>
          <p className={styles.project_link}><a href="https://www.amberskymarketing.com" target="_blank"><u>View live website.</u></a></p>
        </div>
      </main>
    </div>)
  );
}
