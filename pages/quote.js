import { useState, useEffect, useRef } from 'react';
import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/quote.module.css'

import Menu from '../components/menu'


export default function Info() {
  const handleSubmit = () => {
    alert("test")
  }

  return (
    (<div className={styles.container}>
      <Head>
        <title>Sebastian Gale - Creative Web Developer | Selected Work</title>
        <meta name="description" key="description" content="I build and design uniquely great websites for businesses and independent professionals. I’m a web developer and designer based out of Victoria, Canada who speaks both English and French. I’ve been doing software development as a hobby for over 7 years now, including 5 years of professional experience."/>

        <link rel="icon" href="/favicon.ico?v=JyAdPKv4m5" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=JyAdPKv4m5"/>
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=JyAdPKv4m5"/>
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=JyAdPKv4m5"/>
        <link rel="manifest" href="/site.webmanifest?v=JyAdPKv4m5"/>
        <link rel="mask-icon" href="/safari-pinned-tab.svg?v=JyAdPKv4m5" color="#808080"/>
        <link rel="shortcut icon" href="/favicon.ico?v=JyAdPKv4m5"/>
        <meta name="msapplication-TileColor" content="#f8f8f8"/>
        <meta name="theme-color" content="#f8f8f8"/>
      </Head>
      <nav className={styles.nav}>
        <span className={styles.logotype}><Link href="/">SG</Link></span>
        <div className={styles.nav_links}>
          <Link href="/selected-work">Selected Work.</Link>
          <Link href="/info">Info.</Link>
          <Link href="/quote" className={styles.current_link}>Get a Quote.</Link>
        </div>
      </nav>
      <main className={styles.main}>
        <h1 className={styles.page_title}>Get a Quote.</h1>

        <form className={styles.form} action="https://formsubmit.co/contact@sebastiangale.ca" method="POST">
          <h2 className={styles.form_title}>First, some quick questions:</h2>
          <span className={styles.form_subtitle}>Unsure? <Link href="/info" className={styles.contact_me}><u>contact me!</u></Link></span>

          <div className={styles.form_question}>
            <label for="name">What's your name?<span className={styles.asterisk}>*</span></label>
            <input type="text" id="name" name="name" required minlength="4" maxlength="30" autofocus />
          </div>

          <div className={styles.form_question}>
            <label for="email">What's your work/business email?<span className={styles.asterisk}>*</span></label>
            <input type="email" id="email" name="email" inputmode="email" autocomplete="email" required />
          </div>

          <div className={styles.form_question}>
            <label for="what">What can I do for you?<span className={styles.asterisk}>*</span></label>
            <textarea type="text" id="what" name="what they want" required minlength="5" placeholder="e.g. A new website, help finishing a website, update an existing site, etc"/>
          </div>

          <div className={styles.form_question}>
            <label for="kind">What kind of website?<span className={styles.asterisk}>*</span></label>
            <textarea type="text" id="kind" name="type of website" required minlength="5" placeholder="e.g. A portfolio website, ecommerce site, business page, etc"/>
          </div>

          <div className={styles.form_question}>
            <label for="pages">Describe the pages you’ll want me to work on<span className={styles.asterisk}>*</span></label>
            <textarea type="text" id="pages" name="what pages" required minlength="5" placeholder="e.g. A contact page with business hours and a map, a restaurant menu page showing available dishes and their prices, etc"/>
          </div>

          <button type="submit" className={styles.submit} onSubmit={handleSubmit}>Submit</button>
        </form>
      </main>
    </div>)
  );
}
