import { useState, useEffect, useRef } from 'react';
import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/info.module.css'

import Menu from '../components/menu'


export default function Info() {
  return (
    (<div className={styles.container}>
      <Head>
        <title>Sebastian Gale - Creative Web Developer | Selected Work</title>
        <meta name="description" key="description" content="I build and design uniquely great websites for businesses and independent professionals. I’m a web developer from Canada who speaks both English and French. I’ve been doing software development as a hobby for over 7 years now, including 5 years of professional experience."/>

        <link rel="icon" href="/favicon.ico?v=JyAdPKv4m5" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=JyAdPKv4m5"/>
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=JyAdPKv4m5"/>
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=JyAdPKv4m5"/>
        <link rel="manifest" href="/site.webmanifest?v=JyAdPKv4m5"/>
        <link rel="mask-icon" href="/safari-pinned-tab.svg?v=JyAdPKv4m5" color="#808080"/>
        <link rel="shortcut icon" href="/favicon.ico?v=JyAdPKv4m5"/>
        <meta name="msapplication-TileColor" content="#f8f8f8"/>
        <meta name="theme-color" content="#f8f8f8"/>
      </Head>
      <nav className={styles.nav}>
        <span className={styles.logotype}><Link href="/">SG</Link></span>
        <div className={styles.nav_links}>
          <Link href="/selected-work">Selected Work.</Link>
          <Link href="/info" className={styles.current_link}>Info.</Link>
          <Link href="/quote">Get a Quote.</Link>
        </div>
      </nav>
      <main className={styles.main}>
        <h1 className={styles.page_title}>Info.</h1>

        <section className={styles.section}>
          <h2 className={styles.section_title}>Let's chat!</h2>
          <div className={styles.contact_items}>
            <span>Email -<br/><a href="mailto:sebastian@suede.agency"><u>sebastian@suede.agency</u></a></span>
            <span>Phone -<br/><a href="tel:+1 250 848 3512"><u>+1 (250) 848-3512</u></a></span>
            <span>Form -<br/><Link href="/quote"><u>Get a quote</u></Link></span>
          </div>
        </section>

        <section className={styles.section}>
          <h2 className={styles.section_title}>A little bit about me</h2>
          <p className={styles.section_p}>I build and design uniquely great websites for businesses and independent professionals. I’m a web developer from Canada who speaks both English and French. I’ve been doing software development as a hobby for over 7 years now, including 5 years of professional experience. These days I’m running my own agency taking on clients directly.</p>
        </section>

        <section className={styles.section}>
          <h2 className={styles.section_title}>Technical Skills</h2>
          <p className={styles.skill_category}><span className={styles.category_name}>Languages:</span> JavaScript, HTML, CSS, Typescript, Liquid, Java, Python, C#, PHP, Bash, ROBOTC</p>
          <p className={styles.skill_category}><span className={styles.category_name}>Frameworks/Libraries:</span> Next.js, React, React Native, Node.js, Ionic,  UI Kitten, Bootstrap, Material UI, jQuery</p>
          <p className={styles.skill_category}><span className={styles.category_name}>Technologies:</span> Storyblok, MongoDB, MySQL, Git,  JSON, JSON Web Tokens, Unix systems, Shopify, WordPress, Unity</p>
          <p className={styles.skill_category}><span className={styles.category_name}>Tools:</span> Figma, InVision, Adobe Photoshop, Adobe Illustrator</p>
        </section>
      </main>
    </div>)
  );
}
