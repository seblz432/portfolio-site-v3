import Link from 'next/link'
import { useRouter } from 'next/router'
import { useState, useEffect } from 'react'

export default function Menu() {
  const router = useRouter()
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(!open);
  }

  const handleNavigation = (url) => {
    router.push(url).then(() => {
      setOpen(false);
    });
  }

  return (
    <div className={"mobile_menu_container"}>
      <svg className={"menu_icon ham hamRotate ham8 " + ((open) ? 'active' : '')} viewBox="0 0 100 100" width="80" onClick={handleClick}>
        <path
          className="line top"
          d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"
        />
        <path
          className="line middle"
          d="m 30,50 h 40"
        />
        <path
          className="line bottom"
          d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"
        />
      </svg>

      <div className={"mobile_menu " + ((open) ? 'active' : '')}>
        <span className="mobile_menu_title">Menu.</span>

        <div className="mobile_nav_links">
          <a
            className={ (router.pathname == "/") ? 'current_link' : '' }
            onClick={() => {handleNavigation("/")}}
          >
            Home.
          </a>
          <a
            className={ (router.pathname == "/selected-work") ? 'current_link' : '' }
            onClick={() => {handleNavigation("/selected-work")}}
          >
            Selected Work.
          </a>
          <a
            className={ (router.pathname == "/info") ? 'current_link' : '' }
            onClick={() => {handleNavigation("/info")}}
          >
            Info.
          </a>
          <a
            className={ (router.pathname == "/quote") ? 'current_link' : '' }
            onClick={() => {handleNavigation("/quote")}}
          >
            Get a Quote.
          </a>
        </div>
      </div>
    </div>
  )
}
