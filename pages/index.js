import { useState, useEffect, useRef } from 'react';
import Head from 'next/head'
import Link from 'next/link'
import styles from '../styles/home.module.css'

import Menu from '../components/menu'


export default function Home() {

  const handleScroll = () => {
    const position = window.pageYOffset;
    const scrollPercentage = (position / (window.innerHeight * 4));

    const mainSectionPosition = ( 1 - (position / window.innerHeight*2));

    document.documentElement.style.setProperty('--main-section-opacity', mainSectionPosition);
    document.documentElement.style.setProperty('--main-section-display', (mainSectionPosition < 0) ? 'none' : 'flex' );

    document.documentElement.style.setProperty('--canvas-opacity', mainSectionPosition * -2);
    document.documentElement.style.setProperty('--canvas-display', (mainSectionPosition < 0) ? 'block' : 'none' );
  };

  /*const canvas = document.getElementById("canvas");
  let ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);*/

  /*useEffect(() => {
    window.addEventListener('scroll', handleScroll, { passive: true });
    window.addEventListener('resize', handleScroll, { passive: true });

    return () => {
      window.removeEventListener('scroll', handleScroll);
      window.removeEventListener('resize', handleScroll);
    };
  }, []);*/

  return (
    (<div className={styles.container}>
      <Head>
        <title>Sebastian Gale - Creative Web Developer</title>
        <meta name="description" key="description" content="I build and design uniquely great websites for businesses and independent professionals. I’m a web developer and designer based out of Victoria, Canada who speaks both English and French. I’ve been doing software development as a hobby for over 7 years now, including 5 years of professional experience."/>

        <link rel="icon" href="/favicon.ico?v=JyAdPKv4m5" />
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=JyAdPKv4m5"/>
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=JyAdPKv4m5"/>
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=JyAdPKv4m5"/>
        <link rel="manifest" href="/site.webmanifest?v=JyAdPKv4m5"/>
        <link rel="mask-icon" href="/safari-pinned-tab.svg?v=JyAdPKv4m5" color="#808080"/>
        <link rel="shortcut icon" href="/favicon.ico?v=JyAdPKv4m5"/>
        <meta name="msapplication-TileColor" content="#f8f8f8"/>
        <meta name="theme-color" content="#f8f8f8"/>
      </Head>
      <div className={styles.nav}>
        <span className={styles.logotype}><Link href="/">SG</Link></span>
        {/*<span className={styles.language}>En français.</span>*/}
        <div className={styles.nav_links}>
          <Link href="/selected-work">Selected Work.</Link>
          <Link href="/info">Info.</Link>
          <Link href="/quote">Get a Quote.</Link>
        </div>
      </div>
      <main className={styles.main}>
        <div className={styles.main_section}>
          <div className={styles.main_text}>
            <h1 className={styles.main_title}>Hello there!<br/>I’m Sebastian</h1>
            <h2 className={styles.main_subtitle}>I design & build uniquely great websites.</h2>
          </div>
          <Link href="/quote" legacyBehavior><button className={styles.main_button}>Get a quote</button></Link>

          <footer className={styles.footer}>Website  designed and developed by me</footer>
        </div>

        {/*<canvas className={styles.canvas} id="canvas" />
        <img className={styles.canvas_background} src='/video/0001.jpg'/>*/}
      </main>
    </div>)
  );
}
